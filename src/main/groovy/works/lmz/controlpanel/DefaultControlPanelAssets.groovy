package works.lmz.controlpanel

import groovy.transform.CompileStatic
import works.lmz.common.stereotypes.SingletonBean
import works.lmz.controlpanel.core.ControlPanelAssets
import works.lmz.stencil.LinkBuilder

import javax.annotation.PostConstruct
import javax.inject.Inject

/**
 * Author: Marnix
 *
 * Default control panel assets that are available to the user
 */
@SingletonBean
@CompileStatic
class DefaultControlPanelAssets extends ControlPanelAssets {

	@Inject LinkBuilder linkBuilder

	@PostConstruct
	public void init(){
		this.javascripts = [
				linkBuilder.linkTo('app-resources/global.js'),
				linkBuilder.linkTo('app-resources/session.js')
		];
	}


}
